import type {Plugin} from "@stromboli/core";
import {readFileSync} from "fs";

export const createCopyPlugin = (): Plugin => {
    return (context, next) => {
        const {response, component} = context;
        const {name, path} = component;

        let {data} = component;

        if (data === undefined) {
            data = readFileSync(path);
        }

        response.artifacts.push({
            name,
            data
        });

        response.dependencies.push(component.path);

        return next(context);
    };
};