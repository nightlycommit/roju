import type {Plugin} from "@stromboli/core";
import {OutputOptions, rollup, RollupOptions} from "rollup";
import createVirtualPlugin from "@rollup/plugin-virtual";
import {dirname} from "path";
import type {Plugin as RollupPlugin} from "rollup";
// @ts-ignore
import createIncludePathsPlugin from "rollup-plugin-includepaths"; // todo: replace with something serious

export const createRollupPlugin = (
    options: Omit<RollupOptions, "input" | "plugins"> & {
        plugins: Array<RollupPlugin>;
    },
    outputOptions: Omit<OutputOptions, "sourcemap" | "sourcemapExcludeSources">
): Plugin => {
    return (context, next) => {
        const {response, component} = context;
        const {name, path, data} = component;

        if (data) {
            options.plugins = options.plugins || [];

            options.plugins.unshift(
                createIncludePathsPlugin({
                    paths: [dirname(path)]
                })
            );

            options.plugins.unshift(
                createVirtualPlugin({
                    [path]: data.toString()
                })
            );
        }

        const actualOptions: RollupOptions = {
            ...options,
            input: path
        };

        const actualOutputOptions: OutputOptions = {
            ...outputOptions,
            sourcemap: true,
            sourcemapExcludeSources: false
        };

        return rollup(actualOptions)
            .then((build) => {
                return build.generate(actualOutputOptions);
            })
            .then((output) => {
                output.output.map((chunkOrAsset) => {
                    if (chunkOrAsset.type === "asset") {
                        // what are we supposed to do with assets?
                    } else {
                        const {code } = chunkOrAsset;
                        const map = chunkOrAsset.map!;

                        response.artifacts.push({
                            name,
                            data: Buffer.from(code)
                        });

                        if (map.sources.length > 0) {
                            response.dependencies.push(...map.sources);
                        }
                        else {
                            response.dependencies.push(component.path);
                        }
                    }
                });

                return next(context);
            });
    };
};