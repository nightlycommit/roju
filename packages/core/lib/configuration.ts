import type {Component, Plugin, Artifact} from "@stromboli/core";

export type Configuration = {
    outputDirectory: string;
    routes: Array<{
        component: Omit<Component, "path"> & {
            path: string | ((basePath: string) => string)
        };
        plugin: Plugin;
        output: string | ((artifact: Artifact, outputDirectory: string) => string);
    }>;
};