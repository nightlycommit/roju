import {createApplication as createArabesqueApplication} from "@arabesque/core";
import type {Listener, Middleware} from "@arabesque/core";
import type {Context} from "./context";
import {FSWatcher, watch} from "chokidar";
import type {Component} from "./component";
import posix from "path/posix";
import type {Configuration} from "./configuration";
import browserSync from "browser-sync";
import FsExtra from "fs-extra";
import type {Plugin} from "./plugin";
import {createANDMiddleware, createORMiddleware} from "@arabesque/logic-middlewares";

const {emptyDirSync, outputFileSync} = FsExtra;

type Stop = () => Promise<void>;

export type Spawn = (
    workingDirectory: string,
    configuration: Configuration
) => Promise<Stop>;

export type Application = {
    start: () => Promise<Stop>;
    spawn: Spawn;
};

export const createApplication = (
    workingDirectory: string,
    configuration: Configuration,
    options?: {
        buildOnly: boolean;
    }
): Application => {
    const {outputDirectory} = configuration;
    const buildOnly = options?.buildOnly || false;

    emptyDirSync(outputDirectory);

    const browserSyncInstance = browserSync.create();

    const spawn: Spawn = (workingDirectory, configuration) => {
        const {outputDirectory} = configuration;

        const promises = configuration.routes.map((route) => {
            const {component, plugin, output} = route;

            const writeOutput: Plugin = (context, next) => {
                const outputPaths: Array<string> = [];

                for (const artifact of context.response.artifacts) {
                    const outputPath = typeof output === "string" ? `${outputDirectory}/${output}` : `${output(artifact, outputDirectory)}`;

                    outputFileSync(outputPath, artifact.data);

                    if (!outputPaths.includes(outputPath)) {
                        outputPaths.push(outputPath);
                    }
                }

                if (!buildOnly) {
                    browserSyncInstance.reload(outputPaths);
                }

                return next(context);
            };

            const handleError: Plugin = (context, next) => {
                console.error(context.response.error);

                context.response.artifacts = [{
                    name: component.name,
                    data: Buffer.from('')
                }];

                return next(context);
            };

            const middleware = createANDMiddleware(
                createORMiddleware(plugin, handleError),
                writeOutput
            );

            const resolvedComponent = {
                name: component.name,
                path: typeof component.path === "string" ? posix.join(workingDirectory, component.path) : component.path(workingDirectory)
            };

            return middleware({
                component: resolvedComponent,
                response: {
                    artifacts: [],
                    dependencies: []
                }
            }, (context) => {
                return Promise.resolve(context);
            }).then((context) => {
                if (!buildOnly) {
                    const createApplication = (middleware: Middleware<Context>) => {
                        const listener: Listener<{
                            component: Component;
                            filenames: Array<string>
                        }, Context> = ({component, filenames}, handler) => {
                            let watcher: FSWatcher;

                            const watchFiles = (filenames: Array<string>) => {
                                watcher = watch(filenames);

                                watcher.on("change", () => {
                                    return watcher.close()
                                        .then(() => {
                                            return handler({
                                                component,
                                                response: {
                                                    dependencies: [],
                                                    artifacts: []
                                                }
                                            });
                                        })
                                        .then((context) => {
                                            watcher = watchFiles(context.response.dependencies);
                                        });
                                });

                                return watcher;
                            }

                            watcher = watchFiles(filenames);

                            return Promise.resolve(() => watcher.close());
                        };

                        return createArabesqueApplication(listener, middleware);
                    };

                    const application = createApplication(middleware);

                    return application({
                        component: resolvedComponent,
                        filenames: context.response.dependencies
                    });
                }
            });
        });

        return Promise.all(promises)
            .then((stopApplications) => {
                return () => {
                    return Promise.all(stopApplications.map((stop) => {
                        return stop ? stop() : Promise.resolve();
                    })).then();
                };
            });
    }

    return {
        start: () => spawn(workingDirectory, configuration)
            .then((stop) => {
                return new Promise<void>((resolve, reject) => {
                    if (!buildOnly) {
                        browserSyncInstance.init({
                            server: outputDirectory,
                            open: false,
                            notify: false
                        }, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve();
                            }
                        });
                    } else {
                        resolve();
                    }
                }).then(() => {
                    return stop;
                });
            }),
        spawn
    };
};
