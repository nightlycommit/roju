import type {Plugin} from "@stromboli/core";
import {Rebaser} from "css-source-map-rebase";
import type {RebaseHandler} from "css-source-map-rebase";

export const createCssSourceMapRebasePlugin = (
    rebase: RebaseHandler = (_source, _resolvedPath, done) => done()
): Plugin => {
    return (context, next) => {
        return Promise.all(context.response.artifacts.map((artifact) => {
            const {data, sourceMap} = artifact;

            if (sourceMap) {
                const rebaser = new Rebaser({
                    map: Buffer.from(JSON.stringify(sourceMap)),
                    rebase
                });

                rebaser.on('rebase', (_rebasedPath, resolvedPath) => {
                    context.response.dependencies.push(resolvedPath);
                });

                return rebaser.rebase(data).then((result) => {
                    context.response.artifacts.push({
                        name: artifact.name,
                        data: result.css,
                        sourceMap: JSON.parse(result.map.toString())
                    });
                })
            }
        })).then(() => {
            return next(context);
        });
    };
};