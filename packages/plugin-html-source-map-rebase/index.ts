import type {Plugin} from "@stromboli/core";
import {createRebaser} from "html-source-map-rebase";
import type {RebaseHandler} from "html-source-map-rebase";

export const createHtmlSourceMapRebasePlugin = (
    rebase: RebaseHandler = (_source, _resolvedPath, done) => done()
): Plugin => {
    return (context, next) => {
        return Promise.all(context.response.artifacts.map((artifact) => {
            const {data, sourceMap} = artifact;

            if (sourceMap) {
                const rebaser = createRebaser(Buffer.from(JSON.stringify(sourceMap)), {
                    rebase
                });

                rebaser.on('rebase', (_rebasedPath, resolvedPath) => {
                    context.response.dependencies.push(resolvedPath);
                });

                return rebaser.rebase(data).then((result) => {
                    context.response.artifacts.push({
                        name: artifact.name,
                        data: result.data,
                        sourceMap: JSON.parse(result.map.toString())
                    });
                })
            }
        })).then(() => {
            return next(context);
        });
    };
};