import type {Plugin} from "@stromboli/core";
import type {Exception, Options, CompileResult} from "sass";
import {compile, compileString} from "sass";
import {dirname} from "path";

export const createSassPlugin = (
    options?: Options<"sync">
): Plugin => {
    return (context, next) => {
        const {component} = context;
        const {name, path, data} = component;

        try {
            let result: CompileResult;

            const actualOptions = options || {};

            actualOptions.sourceMap = true;
            actualOptions.sourceMapIncludeSources = true;

            if (data) {
                actualOptions.loadPaths = actualOptions.loadPaths || [];
                actualOptions.loadPaths.push(dirname(path));

                result = compileString(data.toString(), actualOptions);
            }
            else {
                result = compile(path, actualOptions);
            }

            context.response.dependencies.push(...result.loadedUrls.map((loadedUrl) => loadedUrl.pathname));

            context.response.artifacts.push({
                name,
                data: Buffer.from(result.css),
                sourceMap: result.sourceMap
            });
        } catch (error: any) {
            const exception = error as Exception;

            console.error(exception);

            if (exception.span.url) {
                context.response.dependencies.push(exception.span.url.pathname);
            }

            context.response.artifacts = [{
                name,
                data: Buffer.from('')
            }];

            context.response.error = error;
        }

        return next(context);
    }
};