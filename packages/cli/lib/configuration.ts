import type {Application, Configuration as CoreConfiguration} from "@stromboli/core";

export type Configuration = CoreConfiguration & {
    onApplicationReady?: (application: Application) => void;
};