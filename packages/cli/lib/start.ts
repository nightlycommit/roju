import {createApplication} from "@stromboli/core";
import findupSync from "findup-sync";
import {resolve} from "path";
import type {Configuration} from "./configuration";

const configurationCandidates = [
    'stromboli.configuration.mjs',
    'stromboli.configuration.cjs',
    'stromboli.configuration.js'
];

const resolveConfiguration = (configurationPath: string | null = null): Promise<Configuration | null> => {
    if (configurationPath === null) {
        configurationPath = findupSync(configurationCandidates);
    }

    if (configurationPath === null) {
        return Promise.resolve(null);
    }

    const resolvedConfigurationPath = resolve(configurationPath);

    console.log('RESOLVED CONF PATH', resolvedConfigurationPath);

    return import(resolvedConfigurationPath)
        .then((module: { default: Configuration }) => module.default);
};

export const start = (workingDirectory: string, options: {
    configurationPath?: string;
    buildOnly: boolean;
}) => {
    const {configurationPath, buildOnly} = options;

    return resolveConfiguration(configurationPath)
        .then((configuration) => {
            if (configuration === null) {
                throw new Error('Invalid configuration');
            }

            const application = createApplication(workingDirectory, configuration, {
                buildOnly
            });

            if (configuration.onApplicationReady) {
                configuration.onApplicationReady(application);
            }

            return application.start();
        });
};