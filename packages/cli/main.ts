import {Command, createArgument, createOption} from "commander";
import {start} from "./lib/start";

export const main = (argv: readonly string[]) => {
    const program = new Command('stromboli')
        .addArgument(createArgument('workingDirectory').argOptional().default('.'))
        .addOption(createOption('-c, --configuration-path <configurationPath>'))
        .addOption(createOption('-b, --build-only').default(false))
        .action((workingDirectory: string, options: {
            configurationPath?: string;
            buildOnly: boolean
        }) => {
            return start(workingDirectory, options).then();
        });

    program.parse(argv);
}
