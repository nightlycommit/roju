import {default as foo} from "./foo.plugin.mjs";

/**
 * @type {Configuration}
 */
const configuration = {
    outputDirectory: 'www',
    routes: [
        {
            component: {
                path: `index.html.twig`,
                name: 'content'
            },
            plugin: foo,
            output: 'index.html'
        }
    ]
};

export {configuration as default};