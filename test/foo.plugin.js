const fooPlugin = (context, next) => {
    return next(context);
};

module.exports = {
    default: fooPlugin
};