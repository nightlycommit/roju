import tape from "tape";
import {createSassPlugin} from "@stromboli/plugin-sass";

tape('SASS Plugin', ({test}) => {
    test('with data', ({same, end}) => {
        const plugin = createSassPlugin({
            style: "compressed"
        });

        return plugin({
            component: {
                name: 'foo',
                path: 'bar',
                data: Buffer.from('.foo {color: red;}')
            },
            response: {
                dependencies: [],
                artifacts: []
            }
        }, (context) => {
            return Promise.resolve(context);
        }).then((context) => {
            same(context.response.artifacts[0].data.toString(), `.foo{color:red}`);

            end();
        });
    })
});