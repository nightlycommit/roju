import {readFileSync} from "fs";

/**
 * @param context {Context}
 * @param next
 * @returns {Promise<Context>}
 */
const fooPlugin = (context, next) => {
    context.response.artifacts = [{
        data: readFileSync(context.component.path),
        name: 'foo'
    }];

    context.response.dependencies = [context.component.path];

    return next(context);
};

export {fooPlugin as default};